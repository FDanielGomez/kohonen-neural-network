# Red neuronal de Kohonen
# Autor: Francisco Daniel Gomez Alavarez

import numpy as numpy
import math

class Kohonen:

    def __init__(self,N,M):
        self.W = None
        self.N = N
        self.M = M

    def createW(self):
        self.W = numpy.random.random(size=(self.M,self.N))

    def setW(self,new_W):
        self.W = new_W;

    def distance(self,inVector):
        distanceVector = []
        for i in range(self.M):
            suma = 0
            for j in range(self.N):
                suma += math.pow( inVector[j] - self.W[i][j] , 2 )
            distanceVector.append([suma,i])
        distanceVector.sort()
        return distanceVector

    def updateW(self,neur,inVector,beta):
        for i in range(self.N):
            self.W[neur][i] = self.W[neur][i] + beta * (inVector[i] - self.W[neur][i])
    
    def printW(self):
        for x in self.W:
            print(str(x))
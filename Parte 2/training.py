# Entrenamiento de la red neuronal de kohonen
# Autor: Francisco Daniel Gomez Alvarez

from io import open
import sys
from kohonenBase import Kohonen

def createTrainingVectors(fileName,typeTr):
    file = open(fileName,"r")
    lines = file.readlines()
    trValues = []
    for x in lines:
        strValues = x.split()
        if typeTr == 1:
            trValues.append([int(y) for y in strValues])
        else:
            trValues.append([float(y) for y in strValues])
    file.close()
    return trValues

def writeW(W,file):
    i = 0
    for x in W:
        file.write("W"+ str(i) + ": " + str(x)+ "\n")
        i += 1

def createWFile(W,N,M):
    file = open("wValues.txt","w")
    file.write(str(N) + " " + str(M) +"\n")
    for x in W:
        for y in x:
            file.write( str(y) + " ")
        file.write("\n")
    file.close()

def exchange(indexOld,indexNew,W):
    if indexOld != indexNew:
        aux = W[indexOld]
        W[indexOld] = W[indexNew]
        W[indexNew] = aux



# INICIO

if len(sys.argv) == 5:

    if sys.argv[1] == 'int':
        typeTr = 1
    else:
        typeTr = 0

    print("Iniciando entrenamiento de la red...")
    print("Neuronas en la capa de entrada: " + str(sys.argv[3]))
    print("Neuronas en la capa de salida: " + str(sys.argv[4]))
    print("Archivo de entrnamiento: " + sys.argv[2])
    print("Tipo de dato del vector de entrenamiento: " + sys.argv[1])
    
    trainingVectors = createTrainingVectors(sys.argv[2],typeTr)
    kohonen = Kohonen(int(sys.argv[3]),int(sys.argv[4]))
    kohonen.createW()
    outFile = open("results.txt","w")
    outFile.write("RED NEURONAL DE KOHONEN\n\n")
    outFile.write("RESULTADOS DEL ENTRENAMIENTO\n\n")
    outFile.write("Valores iniciales de los pesos:\n")
    writeW(kohonen.W,outFile)

    for i in range(500):
        outFile.write("\nITERACION " + str(i) +":\n")
        indexOld = 0
        for x in trainingVectors:
            #outFile.write("\nENTRADA: " + str(x) + "\n")
            distances = kohonen.distance(x)
            #outFile.write("Distancias calculadas: " + str(distances) + "\n")
            #outFile.write("Actualizando pesos de la neurona de salida " + str(distances[0][1]) + ":\n")
            #outFile.write("Valores antiguos: " + str(kohonen.W[distances[0][1]]) + "\n")
            kohonen.updateW(distances[0][1],x,(1/(i+1)))
            #outFile.write("Valores nuevos: " + str(kohonen.W[distances[0][1]]) + "\n")
            exchange(indexOld,distances[0][1],kohonen.W)
            indexOld += 1
        outFile.write("Valores de los pesos:\n")
        writeW(kohonen.W,outFile)
    outFile.close()

    createWFile(kohonen.W,kohonen.N,kohonen.M)
    print("Entrenamiento terminado!")
    print("Archivo de resultados generado: results.txt")
    print("Archivo de pesos generado: wValues.txt (NO BORRAR)")
else:
    print("Argument error!")
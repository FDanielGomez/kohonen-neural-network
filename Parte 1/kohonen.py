# Entrenamiento de la red neuronal de kohonen
# Autor: Francisco Daniel Gomez Alvarez

from io import open
from kohonenBase import Kohonen
import sys
from matplotlib import pyplot as plt

def createKohonen():
    file = open("wValues.txt","r")
    lines = file.readlines()
    nm = lines[0].split();
    end = len(lines)
    wValues = []
    for i in range(1,end):
        strValues = lines[i].split()
        wValues.append([float(y) for y in strValues])
    file.close()
    ko = Kohonen(int(nm[0]),int(nm[1]))
    ko.setW(wValues)
    return ko

def getNeur(W,nSelect):
    xw = []
    yw = []
    i = 0
    for x in W:
        if not i == nSelect:
            xw.append(x[0])
            yw.append(x[1])
        i += 1
    return [xw,yw]



if len(sys.argv) == 3:
    inStr = sys.argv[2].split()
    if sys.argv[1] == 'int':
        Ek = [int(x) for x in inStr]
    else:
        Ek = [float(x) for x in inStr]
    print("Patron de entrada: "+ str(Ek))
    kohonen = createKohonen()
    distances = kohonen.distance(Ek)
    print("Salida: " + str(distances[0][1]))

    neurs = getNeur(kohonen.W,distances[0][1])
    
    plt.plot([Ek[0]],[Ek[1]],'^',label="Punto entrada",color='red')
    plt.plot(neurs[0],neurs[1],'ro',label="Neuronas",color='blue')
    plt.plot([kohonen.W[distances[0][1]][0]],[kohonen.W[distances[0][1]][1]],'s',label="Neurona ganadora",color='green')
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.title("Resultados")
    plt.legend(loc=1)
    plt.grid()
    plt.show()

else:
    print("Argument error!")

